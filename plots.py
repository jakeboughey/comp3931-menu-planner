import matplotlib.pyplot as plt
import matplotlib.transforms as transforms
import numpy as np
import numpy.typing

from models.experiment import ExperimentConfig


def plot_convergence(
    config: ExperimentConfig,
    outfile: str,
    mean_best: numpy.typing.ArrayLike,
    min_best: numpy.typing.ArrayLike,
    max_best: numpy.typing.ArrayLike,
    optimal_fitness: float = None
) -> None:
    '''
    Plot the convergence of an experiment given its config data and raw output data,
    saving an SVG file to `outfile`.
    '''
    # Get blank plot
    _, ax = plt.subplots()

    # Set title, labels
    ax.set_title(
        f'Convergence for \nn_days={config["n_days"]}, n_meals={config["n_meals"]}, n_gens={config["n_gens"]}, n_tries={config["n_tries"]}, pop_size={config["population_size"]}')
    ax.set_xlabel('Generation')
    ax.set_ylabel('Waste (g)')

    trans = transforms.blended_transform_factory(
        ax.get_yticklabels()[0].get_transform(), ax.transData)

    # Plot optimal fitness baseline if desired
    if optimal_fitness is not None:
        ax.axhline(y=optimal_fitness, color='green',
                   linestyle='--', label='Global optimum')
        ax.text(1.04, optimal_fitness,
                f'{optimal_fitness:.3g}g', color='green', transform=trans, ha='left', va='center')

    # Plot average best fitness, and range between min and max best values
    # over generations
    gens = np.arange(config['n_gens'] + 1)

    ax.plot(gens, mean_best, label='Average best fitness')
    ax.fill_between(gens, min_best, max_best, alpha=0.2)

    best = mean_best[-1]

    # Unless we show the global optimum and this is equal to the best converged fitness,
    # label the best converged fitness
    # (condition prevents overlap of labels)
    if optimal_fitness is None or best != optimal_fitness:
        ax.text(1.04, best,
                f'{best:.3g}g', color='C0', transform=trans, ha='left', va='center')

    # Show plot with legend
    plt.legend()
    plt.savefig(outfile)
