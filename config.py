import json

from models.plan import Horizon, SerializedHorizon, FULL_HORIZON
from models.recipe import Recipe
from models.inventory import Inventory, SerializedInventory
from db import db


def deserialize_inventory(file_name: str) -> Inventory:
    '''
    Return a deserialized Inventory object read from the given file.
    '''
    recipes = db.comp3931_menu_planner.recipes

    with open(file_name, 'r') as f:
        raw: SerializedInventory = json.loads(f.read())
        return {
            'ingredients': raw['ingredients'],
            'recipes': [
                {
                    'recipe': Recipe(recipes.find_one({'id': recipe['id']}), was_in_inventory=True),
                    'portions': recipe['portions'],
                    'use_by': recipe['use_by']
                }
                for recipe in raw['recipes']
            ]
        }


def deserialize_horizon(file_name: str) -> Horizon:
    '''
    Return a deserialized Horizon object read from the given file.
    '''
    with open(file_name, 'r') as f:
        raw: SerializedHorizon = json.loads(f.read())

        if 'blacklist' in raw and 'whitelist' in raw:
            raise ValueError(
                'Horizon file must specify either whitelist or blacklist, not both')

        if 'whitelist' in raw:
            return [(time[0], time[1]) for time in raw['whitelist']]
        elif 'blacklist' in raw:
            blacklist = raw['blacklist']
            return [(day, meal) for day, meal in FULL_HORIZON if [day, meal] not in blacklist]
        else:
            raise ValueError(
                'Horizon file must contain either whitelist or blacklist')
