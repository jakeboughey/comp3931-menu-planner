import time
import argparse

from models.inventory import Inventory, EMPTY_INVENTORY
from models.plan import Horizon
from optimize import find_optimal_plan, evaluate_plan_waste
from config import deserialize_inventory, deserialize_horizon
from display import *

parser = argparse.ArgumentParser(
    description='Generate minimal-waste menu plans.',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('-n', dest='n', type=int,
                    default=100, help='Population size')
parser.add_argument('--seed', dest='seed', type=int,
                    default=int(time.time()), help='Random seed')
parser.add_argument('--inventory', dest='inventory_path',
                    type=str, default=None, help='Path to inventory file')
parser.add_argument('--horizon', dest='horizon_path',
                    type=str, default=None, help='Path to horizon file')
parser.add_argument('--waste-weight', dest='waste_weight', type=float, default=1.0,
                    help='Weighting of waste objective (1.0 means minimise with weight 1)')
parser.add_argument('--variety-weight', dest='variety_weight', type=float, default=-1000.0,
                    help='Weighting of variety objective')

args = parser.parse_args()

print(
    f"Using seed: {args.seed} (pass this as an argument if you want to reproduce a result)")

inv: Inventory = EMPTY_INVENTORY if args.inventory_path is None else deserialize_inventory(
    args.inventory_path)
horizon: Horizon = None if args.horizon_path is None else deserialize_horizon(
    args.horizon_path)

hof, pop, log = find_optimal_plan(
    inv,
    horizon,
    objective_weights=(args.waste_weight, args.variety_weight),
    n=args.n,
    seed=args.seed,
    verbose=True
)
best = hof[0]

print(
    f"================ FINAL WASTE: {evaluate_plan_waste(best)}g ================")

print("================ BEST PLAN ================")
show_plan_summary(best)
print()

print("============ LEFTOVER RECIPES ============")
show_leftover_recipes(best)
print()

print("================ INGREDIENTS ================")
show_plan_ingredients(best)
print()

print("============== SHOPPING LIST =============")
show_shopping_list(best)
print()

print("============ LEFTOVER INGREDIENTS ============")
show_leftover_ingredients(best)
print()

print("============ UPDATED INVENTORY ============")
show_new_inventory(best, inv)
print()

print('\n\n')
print("============ UPDATED INVENTORY DUMP ============")
show_new_inventory_repr(best, inv)
