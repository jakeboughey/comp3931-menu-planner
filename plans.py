import typing
from math import ceil

from models.plan import *
from models.recipe import *
from models.inventory import *
from util import unique_by_key, next_multiple


def get_aggregate_ingredients(plan: Plan, inventory: Inventory = None) -> typing.List[MeasuredIngredient]:
    '''
    Return set of aggregate ingredient amounts for a given plan, i.e. the total
    amount of each ingredient needed to make all recipes in the plan,
    excluding ingredient amounts already in the inventory.
    '''
    if inventory is None:
        inventory = {'ingredients': {}}

    # Get unique solution recipes
    solution_recipes = unique_by_key(
        [item.recipe for item in plan], key=lambda recipe: recipe['id'])

    # Collect ingredient amounts across all recipes
    ingredient_amounts: typing.Dict[str, typing.List[float]] = {}

    # Get quantities of each ingredient across recipes
    for recipe in solution_recipes:
        batches = ceil(sum(
            1 for item in plan if item.recipe['id'] == recipe['id']) / recipe['serves'])
        for ing in recipe['ingredients']:
            ing_name = ing['name']
            quantity = ing['quantity']['grams'] * batches

            # Add quantity to relevant bucket
            if ing_name not in ingredient_amounts:
                ingredient_amounts[ing_name] = []

            ingredient_amounts[ing_name].append(quantity)

    return [
        (ing_name, max(sum(quantities) -
         inventory['ingredients'].get(ing_name, 0), 0))
        for ing_name, quantities in ingredient_amounts.items()
    ]


def get_leftover_recipe_portions(plan: Plan) -> typing.List[LeftoverRecipe]:
    '''
    Return the leftover portion count for each recipe in the plan,
    where this is greater than zero.
    '''
    recipes_by_id = {item.recipe['id']: item.recipe for item in plan}
    recipe_occurrences = {}
    recipe_use_by = {}

    for item in plan:
        rid = item.recipe['id']
        recipe_occurrences[rid] = recipe_occurrences.get(rid, 0) + 1
        recipe_use_by[rid] = (Day[item.day] + 3) % Day.SUN

    leftovers: typing.List[LeftoverRecipe] = [
        {
            'recipe': recipes_by_id[rid],
            'portions': next_multiple(occs, recipes_by_id[rid]['serves']) - occs,
            'use_by': recipe_use_by[rid]
        }
        for rid, occs in recipe_occurrences.items()
    ]

    return [item for item in leftovers if item['portions'] > 0]
