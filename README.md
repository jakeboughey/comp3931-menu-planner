# COMP3931 Menu Planner

## Getting started

The solver requires Python 3.8. To install the solver, run

```shell
$ git clone https://gitlab.com/jakeboughey/comp3931-menu-planner
$ cd comp3931-menu-planner
$ virtualenv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

Before the solver can run, please ensure MongoDB is installed and the database has been loaded into MongoDB (under the database name `comp3931_menu_planner`; this can be done using [`mongorestore`](https://www.mongodb.com/docs/database-tools/mongorestore/)). Then, create a file `.env` in the project root, and set the variable `MONGO_CONNECTION=<connection string to the database>`.

Once the solver is installed and the environment is set up, the solver can be invoked by running `python main.py` with appropriate arguments. For usage, run `python main.py -h`.
