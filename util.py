import typing
import functools
import itertools
import random

import numpy
import numpy.typing


memoized = functools.lru_cache(maxsize=None)

T = typing.TypeVar('T')


def flat(arr):
    '''
    Given a nested iterable arr, return a flattened version.
    '''
    return list(itertools.chain.from_iterable(arr))


def clamp(x: float, xmin: float, xmax: float) -> float:
    '''
    Given a value x with xmin <= x <= xmax, return x scaled to a value between 0 and 1.
    '''
    return (x - xmin) / (xmax - xmin)


def unique_by_key(lst: typing.Iterable[T], key: typing.Callable[[T], typing.Any]) -> typing.List[T]:
    '''
    Given a list of items and a key function, return a list of unique items in the list
    based on the key function.

    https://stackoverflow.com/questions/10024646/how-to-get-list-of-objects-with-unique-attribute

    >>> class Obj: pass
    >>> x, y, z = Obj(), Obj(), Obj()
    >>> x.id = y.id = 1
    >>> z.id = 2
    >>> x != y
    True
    >>> x.id == y.id
    True
    >>> unique_by_key([x, y, z], lambda obj: obj.id) == [x, z]
    True
    '''
    seen = set()
    return [seen.add(key(obj)) or obj for obj in lst if key(obj) not in seen]


def random_choice(xs: typing.Iterable[T]) -> typing.Optional[T]:
    '''
    Make a random choice from xs, returning None if xs is empty.
    '''
    return None if len(xs) == 0 else random.choice(xs)


def next_multiple(x: float, base: float) -> float:
    '''
    Return the next multiple of base greater than or equal to x.

    https://stackoverflow.com/questions/2403631/how-do-i-find-the-next-multiple-of-10-of-any-integer
    '''
    return x if x % base == 0 else x + (base - x % base)


def sum_to_one(xs: numpy.typing.ArrayLike) -> numpy.typing.ArrayLike:
    '''
    Return a scaled version of the given iterable such that the new
    iterable's items sum to 1.
    '''
    return xs / numpy.sum(xs)
