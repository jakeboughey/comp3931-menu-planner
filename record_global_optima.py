import csv
import itertools

from optimize import find_optimal_plan_brute_force, evaluate_plan_waste
from recipes import search_recipes
from models.plan import Day, Meal

outfile = 'optima.csv'

min_days = 1
max_days = 7
all_n_days = range(min_days, max_days + 1)

min_meals = 1
max_meals = 3
all_n_meals = range(min_meals, max_meals + 1)

max_horizon_size = 5

recipes_per_meal = 150
recipes_by_meal = {meal: search_recipes(
    meal_type=meal, limit=recipes_per_meal) for meal in Meal.__members__.keys()}

with open(outfile, 'r+') as f:
    reader = csv.reader(f)
    next(reader, None)  # skip header
    existing_entries = [tuple(map(float, row)) for row in reader]
    print(existing_entries)

    for n_meals, n_days in itertools.product(all_n_meals, all_n_days):
        days = list(Day.__members__.keys())[:n_days]
        meals = list(Meal.__members__.keys())[:n_meals]
        horizon = list(itertools.product(days, meals))

        print(
            f'n_days={n_days}, n_meals={n_meals}, recipes_per_meal={recipes_per_meal}')
        if len(horizon) > max_horizon_size:
            print('Horizon too large, skipping')
            continue
        elif any(entry[:3] == (n_days, n_meals, recipes_per_meal) for entry in existing_entries):
            print('Value already computed, skipping')
            continue

        optimum = find_optimal_plan_brute_force(
            horizon, recipes_by_meal=recipes_by_meal, verbose=True)
        f.write(
            f'{n_days},{n_meals},{recipes_per_meal},{evaluate_plan_waste(optimum)}\n')
        f.flush()
