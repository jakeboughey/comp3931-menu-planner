import typing
import random
import math
from functools import partial

import numpy
from deap import base, creator, tools, algorithms
from tqdm import tqdm

import models.plan as plan
import models.recipe as recipe
import models.inventory as inventory
from plans import get_aggregate_ingredients
from products import get_products_for_ingredients
from recipes import search_recipes
from util import *

creator.create('FitnessMin', base.Fitness, weights=(-1.0,))
creator.create('Individual', list, fitness=creator.FitnessMin)


def inventory_use_percentage(
    recipe: recipe.Recipe,
    inventory: inventory.Inventory,
    alpha=5
) -> float:
    '''
    Calculate a recipe selection weight based on the extent to which
    the recipe could use up inventory ingredients.
    '''
    # Calculate total mass in inventory; we can't use up any more than this
    # amount
    total_inventory_mass = sum(
        inv_amount for inv_amount in inventory['ingredients'].values())

    if total_inventory_mass == 0:
        return 1

    # Calculate total used up, total extra
    total_used_up = 0

    for ingredient in recipe['ingredients']:
        if ingredient['name'] in inventory['ingredients']:
            amount_available = inventory['ingredients'][ingredient['name']]
            amount_needed = ingredient['quantity']['grams']

            total_used_up += min(amount_available, amount_needed)

    # Find total used as a percentage of the total mass
    pc_used = total_used_up / total_inventory_mass

    weight = math.exp(alpha * pc_used)
    return weight


def generate_plan_items(
    inventory: inventory.Inventory,
    horizon: plan.Horizon,
    all_recipes: typing.List[recipe.Recipe]
) -> typing.Generator[plan.PlanItem, None, None]:
    '''
    Yield a feasible sequence of `PlanItem`s covering the whole horizon.
    '''
    # Initially all possible (day, meal) pairs of the horizon are uncovered
    uncovered_times = horizon.copy()

    # Recipe selection should be weighted such that recipes using
    # more of the inventory are more likely to be selected
    recipe_weights = numpy.array([inventory_use_percentage(
        recipe, inventory) for recipe in all_recipes])

    # Build pool of available inventory recipe portions
    inventory_recipes = sorted(
        inventory['recipes'], key=lambda item: item['use_by'])

    # Yield `PlanItem`s until all times in the plan have been covered
    while len(uncovered_times) > 0:
        # Choose a random recipe using inventory weighting, and
        # calculate earliest/latest time it should be used

        # Earliest day the recipe can appear is the earliest uncovered day
        if len(inventory_recipes) > 0:
            # Choose recipe in inventory that should be used soonest
            leftover = inventory_recipes.pop(0)
            recipe = leftover['recipe']
            portions = leftover['portions']
            compatible_times = [
                plan.Day[day] for day, meal in uncovered_times if meal in recipe['meal_types']]
            if compatible_times == []:
                continue
            earliest_day = min(compatible_times)
            latest_day = leftover['use_by']
        else:
            recipe = numpy.random.choice(
                all_recipes, p=sum_to_one(recipe_weights))
            compatible_times = [
                plan.Day[day] for day, meal in uncovered_times if meal in recipe['meal_types']]
            if compatible_times == []:
                continue
            earliest_day = min(compatible_times)

            max_portions = int(max(recipe['serves'], math.floor(
                len(uncovered_times) / recipe['serves'])))
            portions_range = range(1, max_portions + 1)
            portions_weights = numpy.array(
                [math.exp(-idx) for idx, _ in enumerate(portions_range, start=1)])

            portions = recipe['serves'] * \
                numpy.random.choice(
                    portions_range, p=sum_to_one(portions_weights))
            # latest is 3 days later since leftovers generally last 3 days post-cook
            latest_day = earliest_day + 3

        recipe_meals = recipe['meal_types']

        # Determine times the recipe can 'slot' into
        # Then on these days, we can schedule on appropriate meals
        best_case_times = [
            time for time in uncovered_times
            if time[1] in recipe_meals and earliest_day <= plan.Day[time[0]] <= latest_day
        ]

        # If we can't cover any times, try again
        if len(best_case_times) == 0:
            continue

        # Place initial serving as soon as possible, then
        # randomly spread out later servings before they expire
        case_times = [best_case_times.pop(0), *random.sample(best_case_times, k=min(
            len(best_case_times), int(portions - 1)))]

        for day, meal in case_times:
            uncovered_times.remove((day, meal))
            yield plan.PlanItem(day=day, meal=meal, recipe=recipe)


def generate_feasible_plan(
    inventory: inventory.Inventory,
    horizon: plan.Horizon,
    all_recipes: typing.List[recipe.Recipe]
) -> 'creator.Individual':
    '''
    Generate a random feasible plan over the horizon based on the given inventory.
    '''
    # Bind the inventory as an argument to generate_plan_items, and build
    # a plan list by iterating over this bound generator function
    plan = tools.initIterate(list, partial(
        generate_plan_items, inventory, horizon, all_recipes))

    # Sort the plan, first by day then by meal
    plan.sort(key=lambda item: (item.day, item.meal))

    # Get aggregate ingredient quantities from plan recipes
    ingredients = get_aggregate_ingredients(
        plan, inventory)

    # Wrap raw plan as Individual object to be handled by DEAP
    ind = creator.Individual(plan)

    # Get product set from aggregate ingredients
    ind.products = get_products_for_ingredients(ingredients)

    return ind


def evaluate_plan_waste(plan: 'creator.Individual') -> float:
    '''
    Return the waste of the plan, measured as the total difference
    of purchased and required ingredient quantities.
    '''
    return sum(prod['waste'] for prod in plan.products.values())


def evaluate_plan_variety(plan: 'creator.Individual') -> float:
    '''
    Return the variety of the plan, defined as the number of unique recipes
    in the plan divided by the length of the horizon. In the best case,
    the number of unique recipes equals the length of the horizon, i.e.
    a different recipe is consumed each mealtime.
    '''
    return len(set(item.recipe['id'] for item in plan)) / len(plan)


def evaluate_plan(plan: 'creator.Individual', objective_weights: typing.Tuple[float, ...]) -> typing.Tuple[float, float]:
    '''
    Return the fitness tuple for the given plan.
    '''
    return objective_weights[0] * evaluate_plan_waste(plan) + objective_weights[1] * evaluate_plan_variety(plan),


def mutate_plan(all_recipes: typing.List[recipe.Recipe], ind: 'creator.Individual', indpb: float = 0.3) -> typing.Tuple['creator.Individual']:
    '''
    Mutate a plan by replacing each recipe in the plan with a random recipe
    that can occur the same number of times with no leftovers, and whose meal types
    are compatible, with probability prob.
    '''
    # Convert plan to sequence of recipe IDs so recipe occurrences
    # can be counted later
    plan_recipe_ids = [item.recipe['id'] for item in ind]

    # For each recipe r in the plan, find recipes r' whose servings can be scaled
    # to replace *all* occurrences of r, and serve the same meal types
    # so that we might replace r with r'
    potential_mutations = {
        item.recipe['id']: None if item.recipe.was_in_inventory else (random_choice([
            r for r in all_recipes
            if plan_recipe_ids.count(item.recipe['id']) % r['serves'] == 0
            and set(r['meal_types']).issuperset(set(item.recipe['meal_types']))
        ]) if random.random() < indpb else None)
        for item in ind
    }

    # Apply the mutations based on probability param
    for idx, item in enumerate(ind):
        replacement = potential_mutations[item.recipe['id']]
        if replacement is None:
            continue

        ind[idx] = plan.PlanItem(
            day=item.day, meal=item.meal, recipe=replacement)

    ind.products = get_products_for_ingredients(get_aggregate_ingredients(ind))

    return ind,


def crossover_plans(
    all_recipes: typing.List[recipe.Recipe],
    ind1: 'creator.Individual',
    ind2: 'creator.Individual',
    indpb: float = 0.5
) -> typing.Tuple['creator.Individual', 'creator.Individual']:
    '''
    Perform crossover between two plans by finding recipes of compatible meal types
    that occur the same number of times in both plans, and swapping them with probability indpb.
    '''
    recipes_by_id = {r['id']: r for r in all_recipes}

    ind1_recipe_ids = [item.recipe['id'] for item in ind1]
    ind2_recipe_ids = [item.recipe['id'] for item in ind2]

    # Find swaps that can be made from ind1's side, i.e. for each
    # recipe on ind1's side, a 'compatible' recipe in ind2
    # where compatible has the same meaning as in the mutation
    swaps_from_ind1 = {
        item1.recipe['id']: random_choice([
            item2.recipe['id'] for item2 in ind2
            if ind1_recipe_ids.count(item1.recipe['id']) == ind2_recipe_ids.count(item2.recipe['id'])
            and set(item1.recipe['meal_types']) == set(item2.recipe['meal_types'])
        ]) if random.random() < indpb else None
        for item1 in ind1
        if not item1.recipe.was_in_inventory
    }
    # Invert the mapping
    swaps_from_ind2 = {swaps_from_ind1[rid]: rid for rid in swaps_from_ind1}

    # Bring ind2 recipes to ind1
    for idx, item in enumerate(ind1):
        swap = swaps_from_ind1.get(item.recipe['id'], None)
        if swap is None:
            continue

        ind1[idx] = plan.PlanItem(
            day=item.day, meal=item.meal, recipe=recipes_by_id[swap])

    # Bring ind1 recipes to ind2
    for idx, item in enumerate(ind2):
        swap = swaps_from_ind2.get(item.recipe['id'], None)
        if swap is None:
            continue

        ind2[idx] = plan.PlanItem(
            day=item.day, meal=item.meal, recipe=recipes_by_id[swap])

    ind1.products = get_products_for_ingredients(
        get_aggregate_ingredients(ind1))
    ind2.products = get_products_for_ingredients(
        get_aggregate_ingredients(ind2))

    return ind1, ind2


def find_optimal_plan(
    inv: inventory.Inventory,
    horizon: plan.Horizon,
    objective_weights: typing.Tuple[float, ...],
    all_recipes: typing.List[recipe.Recipe] = None,
    n=100,
    mutpb=0.1,
    cxpb=0.8,
    ngen=15,
    tournsize=2,
    seed=None,
    verbose=False,
    hofsize=10
):
    '''
    Find an optimal plan based on a genetic algorithm, given existing
    inventory inv and some horizon. Return the hall of fame (set of best individuals),
    entire population and logbook.
    '''
    if horizon is None:
        horizon = plan.FULL_HORIZON
    assert len(horizon) > 0

    if all_recipes is None:
        all_recipes = flat(search_recipes(meal_type=meal)
                           for meal in plan.Meal.__members__.keys())

    # Seed RNGs for reproducibility
    random.seed(seed)
    numpy.random.seed(seed)

    # Register tools (functions to be called by eaSimple)
    toolbox = base.Toolbox()
    # `individual`: call generate_feasible_plan(inv)
    toolbox.register('individual', generate_feasible_plan,
                     inv, horizon, all_recipes)
    # `population`: build list from repeated calls to `individual`, size TBD
    toolbox.register('population', tools.initRepeat, list, toolbox.individual)

    # Register genetic operators
    toolbox.register('evaluate', evaluate_plan,
                     objective_weights=objective_weights)
    toolbox.register('mate', crossover_plans, all_recipes)
    toolbox.register('mutate', mutate_plan, all_recipes)
    toolbox.register('select', tools.selTournament, tournsize=tournsize)

    # Register objective-value statistics
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean)
    stats.register("std", numpy.std)
    stats.register("min", numpy.min)
    stats.register("max", numpy.max)

    # Create hall of fame of size hofsize
    hof = tools.HallOfFame(hofsize)

    if verbose:
        print(f'Generating initial population (n={n})...', end='', flush=True)
    pop = toolbox.population(n)
    if verbose:
        print('done.')

    # Solve problem via simple EA
    pop, log = algorithms.eaSimple(pop, toolbox, cxpb=cxpb, mutpb=mutpb, ngen=ngen,
                                   stats=stats, halloffame=hof, verbose=verbose)

    return hof, pop, log


def find_optimal_plan_brute_force(
    horizon: plan.Horizon,
    objective_weights: typing.Tuple[float, ...],
    recipes_by_meal: typing.Dict[plan.Meal, typing.List[recipe.Recipe]] = None,
    verbose=False
) -> plan.Plan:
    if recipes_by_meal is None:
        # If needed, split the (entire) recipe set by meal type
        recipes_by_meal = {meal: search_recipes(
            meal_type=meal) for meal in plan.Meal.__members__.keys()}

    # (Lazily) generate all possible recipe sequences for the given horizon
    possible_recipe_seqs = itertools.product(*[
        recipes_by_meal[meal] for _, meal in horizon
    ])
    # Wrap each sequence as a Plan object
    possible_plans = (
        creator.Individual([plan.PlanItem(day=day, meal=meal, recipe=recipe)
                            for (day, meal), recipe in zip(horizon, seq)])
        for seq in possible_recipe_seqs
    )

    # If verbose, show a progress bar as we iterate over all possible
    # plans
    if verbose:
        # Total number of plans is product of number of meals that can be used
        # in each horizon time
        #
        # Counting procedure:
        # select breakfast recipe for (mon, breakfast) (n_breakfast_recipes choices)
        # select lunch recipe for (mon, lunch) (n_lunch_recipes choices)
        # .
        # .
        # .
        total_plans = numpy.prod(
            [len(recipes_by_meal[meal]) for _, meal in horizon])
        possible_plans = tqdm(possible_plans, total=total_plans)

    # Find plan having minimum waste, by brute-force iteration
    best: plan.Plan = None
    best_fitness = math.inf

    for p in possible_plans:
        ingredients = get_aggregate_ingredients(p)
        p.products = get_products_for_ingredients(ingredients)

        fitness = evaluate_plan(p, objective_weights=objective_weights)
        if fitness <= best_fitness:
            best = p
            best_fitness = fitness

    return best
