import json
import os
import csv
import sys
import argparse
import time
import typing
import functools
from itertools import product

from tqdm import tqdm
import numpy as np

import plots
from config import deserialize_inventory
from recipes import search_recipes
from optimize import evaluate_plan, find_optimal_plan, find_optimal_plan_brute_force
from models.experiment import ExperimentConfig, ExperimentInfo
from models.plan import Day, Meal
from models.inventory import EMPTY_INVENTORY
from util import flat


def load_experiment_config(file: str) -> ExperimentConfig:
    '''
    Load experiment configuration data from the given file.
    '''
    with open(file, 'r') as f:
        raw = json.load(f)

        raw['objective_weights'] = tuple(
            raw.get('objective_weights', (1.0, 0.0)))
        if 'verbose' not in raw:
            raw['verbose'] = False
        if not 1 <= raw['n_days'] <= len(Day.__members__):
            raise ValueError(
                f'n_days must be between 1 and {len(Day.__members__)}')
        if not 1 <= raw['n_meals'] <= len(Meal.__members__):
            raise ValueError(
                f'n_days must be between 1 and {len(Meal.__members__)}')
        if raw['recipes_per_meal'] < 1:
            raise ValueError('recipes_per_meal must be at least 1')
        if raw['population_size'] < 1:
            raise ValueError('population_size must be at least 1')
        if raw['n_tries'] < 1:
            raise ValueError('n_tries must be at least 1')
        if raw['n_gens'] < 1:
            raise ValueError('n_gens must be at least 1')

        return raw


def get_precomputed_optimum(
    n_days: int,
    n_meals: int,
    recipes_per_meal: int,
    objective_weights: typing.Tuple[float, tuple],
    infile: str = 'optima.csv'
) -> typing.Optional[float]:
    '''
    Read the precomputed global optimum for the given problem parameters,
    if it exists, from the file `infile`.
    '''
    with open(infile, 'r') as f:
        reader = csv.reader(f)
        next(reader, None)  #  skip header

        for row in reader:
            row = tuple(map(float, row))
            # If first columns match params, return last column (optimum)
            if row[:-1] == (n_days, n_meals, recipes_per_meal, *objective_weights):
                return row[-1]

        return None


def write_experiment_info(info: ExperimentInfo, file: str) -> None:
    '''
    Write experiment info to the given file.
    '''
    with open(file, 'w') as f:
        f.write(json.dumps(info, indent=4))


T = typing.TypeVar('T')


def time_call(f: typing.Callable[[], T]) -> typing.Tuple[T, float]:
    '''
    Make a call to f (must have no arguments), and return the time taken
    to make the call as well as the return value from the call.
    '''
    t0 = time.time()
    retval = f()
    t1 = time.time()

    return retval, t1 - t0


def run_experiment(config: ExperimentConfig, results_path: str) -> None:
    '''
    Run an experiment based on the given config, writing the plot and info file
    to `results_path`.
    '''
    result_folder = os.path.join(
        sys.path[0], results_path, f'{config["name"]}_{int(time.time())}')
    if not os.path.isdir(result_folder):
        os.makedirs(result_folder)

    print(f'Results will be written to {result_folder}/')

    inv = deserialize_inventory(
        config['inventory']) if 'inventory' in config else None

    # Generate horizon from params
    days = list(Day.__members__.keys())[:config['n_days']]
    meals = list(Meal.__members__.keys())[:config['n_meals']]
    horizon = list(product(days, meals))

    recipes_by_meal = {meal: search_recipes(
        meal_type=meal, limit=config['recipes_per_meal']) for meal in meals}

    all_recipes = flat(recipes_by_meal.values())
    assert len(all_recipes) <= config['recipes_per_meal'] * config['n_meals']

    total_plans = np.prod(
        [len(recipes_by_meal[meal]) for _, meal in horizon])
    assert config['population_size'] < total_plans, 'Population size should be less than total plans for fair measure'

    # Find global optimum via brute force
    optimal_fitness = None
    if config['show_global_optimum']:
        precomputed = get_precomputed_optimum(
            config['n_days'], config['n_meals'], config['recipes_per_meal'], config['objective_weights'])
        optimal_fitness = precomputed or evaluate_plan(find_optimal_plan_brute_force(
            horizon, config['objective_weights'], recipes_by_meal, verbose=config['verbose']), config['objective_weights'])

        print(
            f'Optimal fitness with n_days={config["n_days"]}, n_meals={config["n_meals"]}, recipes_per_meal={config["recipes_per_meal"]}, weights={config["objective_weights"]}: {optimal_fitness}g')

    # Run GA specified number of times and save the logbook results from each run
    logbooks = []
    times = []

    for _ in tqdm(range(config['n_tries'])):
        find_plan = functools.partial(
            find_optimal_plan,
            all_recipes=all_recipes,
            objective_weights=config['objective_weights'],
            n=config['population_size'],
            ngen=config['n_gens'],
            horizon=horizon,
            inv=inv or EMPTY_INVENTORY,
            seed=int(time.time())
        )

        result, completed_time = time_call(find_plan)
        times.append(completed_time)
        logbooks.append(result[2])

    mean_best = [np.mean([entry['min'] for entry in gen_entries])
                 for gen_entries in zip(*logbooks)]
    min_best = np.array([np.min([entry['min'] for entry in gen_entries])
                        for gen_entries in zip(*logbooks)])
    max_best = np.array([np.max([entry['min'] for entry in gen_entries])
                         for gen_entries in zip(*logbooks)])

    print('Generating convergence plot...', end='')
    plot_path = os.path.join(result_folder, 'plot.svg')
    plots.plot_convergence(
        config=config,
        optimal_fitness=optimal_fitness,
        mean_best=mean_best,
        min_best=min_best,
        max_best=max_best,
        outfile=plot_path
    )
    print(f'done ({plot_path}).')

    total_time = sum(times)
    info: ExperimentInfo = {
        'original_config': config,
        'global_optimum': optimal_fitness,
        'results': {
            'total_completion_time_secs': total_time,
            'average_completion_time_secs': total_time / config['n_tries'],
            'logbooks': logbooks
        }
    }
    info_path = os.path.join(result_folder, 'info.json')
    write_experiment_info(info, file=info_path)
    print(f'Info written to {info_path}.')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Run experiments on the minimal-waste menu planning solver.')

    parser.add_argument('file', help='Experiment config file', type=str)
    parser.add_argument('-o', dest='results_path', type=str,
                        help='Path to results directory')

    args = parser.parse_args()

    cfg = load_experiment_config(args.file)
    run_experiment(cfg, results_path=args.results_path)
