import json

from tabulate import tabulate
from termcolor import colored

from models.plan import Plan, Day, Meal
from models.inventory import Inventory
from plans import get_aggregate_ingredients, get_leftover_recipe_portions
from util import flat


def show_plan_summary(plan: Plan) -> None:
    '''
    Display a tabular summary of a plan's recipes over each (day, meal).
    '''
    table = [
        ['Day', 'Meal', 'Recipe title'],
        *[
            [day, meal, f'{recipe["title"]} (serves {int(recipe["serves"])})']
            for day, meal, recipe in sorted(plan, key=lambda plan: (Day[plan.day], Meal[plan.meal]))
        ]
    ]

    print(tabulate(table, headers='firstrow'))


def show_shopping_list(plan: Plan) -> None:
    '''
    Display the quantity of each product to be purchased for the given plan.
    '''
    product_quantities = flat(products['products'].items()
                              for products in plan.products.values())
    table = [['Product', 'Quantity'], *sorted([prod['product_name'],
                                               f'x{int(quantity)}'] for prod, quantity in product_quantities if quantity > 0)]

    print(tabulate(table, headers='firstrow'))


def show_leftover_recipes(plan: Plan) -> None:
    '''
    Display the number of leftover portions of each recipe in the plan,
    where they exist.
    '''
    leftover_recipes = get_leftover_recipe_portions(plan)
    table = [('Recipe', 'Leftover portions', 'Use by'), *[(leftover['recipe']['title'], leftover['portions'], Day(leftover['use_by']).name)
                                                          for leftover in leftover_recipes]]

    print(tabulate(table, headers='firstrow'))


def show_plan_ingredients(plan: Plan) -> None:
    '''
    Display the (combined) amount of each ingredient required for all plan recipes.
    '''
    table = [
        ('Ingredient', 'Required quantity'),
        *sorted([
            (ing_name, f'{quantity:.3g}g')
            for ing_name, quantity in get_aggregate_ingredients(plan, inventory=None)
        ])
    ]

    print(tabulate(table, headers='firstrow'))


def show_leftover_ingredients(plan: Plan) -> None:
    '''
    Display the amount leftover of each non-inventory ingredient.
    '''
    table = [
        ('Ingredient', 'Leftover amount'),
        *sorted([
            (ing_name, f'{entry["waste"]:.3g}g')
            for ing_name, entry in plan.products.items()
            if entry['waste'] > 0
        ])
    ]

    print(tabulate(table, headers='firstrow'))


def show_new_inventory(plan: Plan, original_inventory: Inventory) -> None:
    '''
    Display the new state of the inventory following the plan.
    '''
    plan_ingredients = {ing_name: quantity for ing_name,
                        quantity in get_aggregate_ingredients(plan)}
    all_ingredients = {
        *plan_ingredients.keys(), *original_inventory['ingredients'].keys()}

    before_after_table = sorted(
        [(
            ing_name,  # name
            original_inventory['ingredients'].get(ing_name, 0),  # before
            (plan.products[ing_name]['actual_quantity']
             if ing_name in plan.products else 0)
            + original_inventory['ingredients'].get(ing_name, 0)
            - plan_ingredients.get(ing_name, 0)  # after
        ) for ing_name in all_ingredients]
    )

    table = [
        ('Ingredient', 'Amount before', 'Amount after', 'Net change'),
        *[(ing_name, f'{before:.3f}g', f'{after:.3f}g', colored(f'{after - before:+.3f}g', color=('red' if after - before > 0 else ('white' if after - before == 0 else 'green'))))
            for ing_name, before, after in before_after_table]
    ]

    print(tabulate(table, headers='firstrow'))


def show_new_inventory_repr(plan: Plan, original_inventory: Inventory) -> None:
    '''
    Display the new state of the inventory (leftover recipes and ingredients) as a
    JSON object.
    '''
    plan_ingredients = {ing_name: quantity for ing_name,
                        quantity in get_aggregate_ingredients(plan)}
    all_ingredients = {
        *plan_ingredients.keys(), *original_inventory['ingredients'].keys()}
    leftover_recipes = get_leftover_recipe_portions(plan)

    print(json.dumps({
        'ingredients': {
            ing_name: (plan.products[ing_name]['actual_quantity']
                       if ing_name in plan.products else 0)
            + original_inventory['ingredients'].get(ing_name, 0)
            - plan_ingredients.get(ing_name, 0)
            for ing_name in all_ingredients
        },
        'recipes': [
            {'id': leftover['recipe']['id'], 'portions': leftover['portions'], 'use_by': leftover['use_by']} for leftover in leftover_recipes
        ]
    }))
