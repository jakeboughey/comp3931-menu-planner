import os

import pymongo
from dotenv import load_dotenv

# Load environment variables
load_dotenv()

# Export Mongo connection
db = pymongo.MongoClient(os.getenv('MONGO_CONNECTION'))
