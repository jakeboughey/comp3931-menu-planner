from typing import *

from ortools.linear_solver import pywraplp

from models.product import *
from models.recipe import *
from util import *
from db import db


class NoOptimalProductSetException(Exception):
    pass


ALL_PRODUCTS_BY_INGREDIENT = list(db.comp3931_menu_planner.products.find(
    {}, {k: 1 for k in RawProduct.__annotations__.keys()}))


@memoized
def get_products_by_name(ing_name: str) -> List[Product]:
    '''
    Return a list of `Product`s matching the ingredient name.
    '''
    return list(map(Product, [prod for prod in ALL_PRODUCTS_BY_INGREDIENT if ing_name in prod['matched_ingredients']]))


ProductId = str


@memoized
def select_products(ing_name: str, required_amount: float) -> ProductSelection:
    '''
    Given an ingredient name and required amount, return an optimal product set,
    i.e. a set of products whose total quantity exceeds, and is as close as
    possible to, the required amount.
    '''
    solver = pywraplp.Solver.CreateSolver('SCIP')
    infinity = solver.infinity()

    products = get_products_by_name(ing_name)
    products_by_id = {prod['id']: prod for prod in products}

    # Create one quantity variable per product
    selected_quantities = {product['id']: solver.IntVar(0.0, infinity, product['id'])
                           for product in products}

    # Find total mass of selected products
    selected_size = sum(
        selected_quantity * products_by_id[prod_id]['gram_weight'] for prod_id, selected_quantity in selected_quantities.items())

    # Total mass should exceed and be as close to required amount as possible
    solver.Add(selected_size >= required_amount)
    solver.Minimize(selected_size - required_amount)

    status = solver.Solve()

    if status == pywraplp.Solver.OPTIMAL:
        products = {
            products_by_id[prod_id]: variable.solution_value()
            for prod_id, variable in selected_quantities.items()
        }
        absolute_waste = solver.Objective().Value()

        return {
            'required_quantity': required_amount,
            'actual_quantity': absolute_waste + required_amount,
            'products': products,
            'waste': absolute_waste,
        }
    else:
        raise NoOptimalProductSetException(
            'Could not find optimal product set')


def get_products_for_ingredients(ingredients: List[MeasuredIngredient]) -> Dict[str, ProductSelection]:
    '''
    Given a list of `MeasuredIngredient`s, return an optimal `ProductSelection`
    for each ingredient.
    '''
    return {name: select_products(name, quantity) for name, quantity in ingredients}
