import typing

from .recipe import Recipe
from .plan import Day

IngredientName = str
GramsAmountInInventory = float
PortionsInInventory = int


class LeftoverRecipe(typing.TypedDict):
    recipe: Recipe
    portions: PortionsInInventory
    use_by: Day


class SerializedLeftoverRecipe(typing.TypedDict):
    id: str
    portions: PortionsInInventory
    use_by: Day


class Inventory(typing.TypedDict):
    ingredients: typing.Dict[IngredientName, GramsAmountInInventory]
    recipes: typing.List[LeftoverRecipe]


EMPTY_INVENTORY: Inventory = {'ingredients': {}, 'recipes': []}


class SerializedInventory(typing.TypedDict):
    ingredients: typing.Dict[IngredientName, GramsAmountInInventory]
    recipes: typing.List[SerializedLeftoverRecipe]
