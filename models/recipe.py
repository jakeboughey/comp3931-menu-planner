import typing


class OriginalIngredientQuantity(typing.TypedDict):
    amount: str
    unit: str


class IngredientQuantity(typing.TypedDict):
    original: OriginalIngredientQuantity
    grams: float


class Ingredient(typing.TypedDict):
    name: str
    quantity: IngredientQuantity


MeasuredIngredient = typing.Tuple[str, float]


class RawRecipe(typing.TypedDict):
    id: str
    title: str
    ingredients: typing.List[Ingredient]
    meal_types: typing.List[str]

    # Based on assumption that 1 serving = 420g of food,
    # see https://preprod.wrap.org.uk/system/files/2020-09/WRAP-Expressing%20redistributed%20food%20surplus%20as%20meal%20equivalents%20%28WRAP%20guidance%29.pdf
    serves: int


class Recipe:
    _raw_recipe: RawRecipe
    was_in_inventory: bool

    def __init__(self, raw_recipe: RawRecipe, was_in_inventory: bool = False) -> None:
        raw_recipe['meal_types'] = [meal.upper()
                                    for meal in raw_recipe['meal_types']]
        self._raw_recipe = raw_recipe
        self.was_in_inventory = was_in_inventory

    def __getitem__(self, k: str) -> typing.Any:
        return self._raw_recipe[k]  # type: ignore

    def __hash__(self) -> int:
        return hash(self._raw_recipe['id'])

    def __eq__(self, other: object) -> bool:
        return isinstance(other, Recipe) and self['id'] == other['id']

    def __str__(self) -> str:
        return str(self._raw_recipe)

    def __repr__(self) -> str:
        return f'<Recipe({self["id"]}, {self["title"]})>'
