import typing


class ExperimentConfig(typing.TypedDict):
    # Name of experiment
    name: str
    # Number of days used in the horizon
    n_days: int
    # Number of meals used in the horizon
    n_meals: int
    # Number of recipes (max) to consider per meal
    recipes_per_meal: int
    # Size of initial GA population
    population_size: int
    # Number of tries to run the GA
    n_tries: int
    # Number of generations the GA should produce before stopping
    n_gens: int
    # Whether to show (and potentially compute) the global optimum
    # fitness
    show_global_optimum: bool
    # Path to inventory file to use, if any
    inventory: typing.Optional[str]
    # Objective weights
    objective_weights: typing.List[float]


class ExperimentResults(typing.TypedDict):
    total_completion_time_secs: float
    average_completion_time_secs: float
    logbooks: list


class ExperimentInfo(typing.TypedDict):
    original_config: ExperimentConfig
    global_optimum: float
    results: ExperimentResults
