import typing


class RawProduct(typing.TypedDict):
    id: str
    product_name: str
    # countries: str
    # ingredients_hierarchy: str
    # popularity_tags: typing.List[str]
    # categories_hierarchy: typing.List[str]
    # completeness: float
    # data_quality_tags: typing.List[str]
    # stores: str
    matched_ingredients: typing.List[str]
    gram_weight: float


class Product:
    _raw_product: RawProduct

    def __init__(self, raw_product: RawProduct) -> None:
        self._raw_product = raw_product

    def __getitem__(self, k: str) -> typing.Any:
        return self._raw_product[k]  # type: ignore

    def __hash__(self) -> int:
        return hash(self._raw_product['id'])

    def __eq__(self, other: object) -> bool:
        return isinstance(other, Product) and self['id'] == other['id']

    def __str__(self) -> str:
        return str(self._raw_product)

    def __repr__(self) -> str:
        return f'<Product({self["id"]}, {self["product_name"]})>'


class ProductSelection(typing.TypedDict):
    products: typing.Dict[Product, int]
    required_quantity: float
    actual_quantity: float
    waste: float
