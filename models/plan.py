import typing
from itertools import product
from enum import IntEnum

from .recipe import Recipe


class Day(IntEnum):
    MON = 0
    TUE = 1
    WED = 2
    THU = 3
    FRI = 4
    SAT = 5
    SUN = 6


class Meal(IntEnum):
    BREAKFAST = 7
    LUNCH = 8
    DINNER = 9


class PlanItem(typing.NamedTuple):
    day: Day
    meal: Meal
    recipe: Recipe

    def __eq__(self, other: typing.Any) -> bool:
        if not isinstance(other, PlanItem):
            return False

        return other.day == self.day and other.meal == self.meal and other.recipe['id'] == self.recipe['id']


Plan = typing.List[PlanItem]

Horizon = typing.List[typing.Tuple[str, str]]
FULL_HORIZON = list(product(Day.__members__, Meal.__members__))


class SerializedHorizonWhitelist(typing.TypedDict):
    whitelist: typing.List[typing.List[str]]


class SerializedHorizonBlacklist(typing.TypedDict):
    blacklist: typing.List[typing.List[str]]


SerializedHorizon = typing.Union[SerializedHorizonBlacklist,
                                 SerializedHorizonWhitelist]
