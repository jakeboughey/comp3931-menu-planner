import typing

from db import db
from models.recipe import Recipe
from models.plan import Meal


def search_recipes(*, limit: int = None, meal_type: Meal = None) -> typing.List[Recipe]:
    '''
    Return a list of recipes from Recipe1M based on kwargs.
    '''
    params = {}

    if meal_type is not None:
        params['meal_types'] = meal_type.lower()

    query = db.comp3931_menu_planner.recipes.find(
        params)
    if limit is not None:
        query.limit(limit)

    matches = list(query)

    # Capitalize to ensure match with Meal literals
    for recipe in matches:
        recipe['meal_types'] = [meal_type.upper()
                                for meal_type in recipe['meal_types']]

    return list(map(Recipe, matches))
