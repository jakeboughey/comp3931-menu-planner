from plans import *

recipes = [
    {
        'id': 1,
        'title': 'Curry',
        'serves': 2,
        'ingredients': [
            {
                'id': 1,
                'name': 'Onion',
                'quantity': {
                    'grams': 100.0
                }
            },
            {
                'id': 2,
                'name': 'Garlic',
                'quantity': {
                    'grams': 30.0
                }
            },
            {
                'id': 3,
                'name': 'Fresh ginger',
                'quantity': {
                    'grams': 10.0
                }
            }
        ]
    },
    {
        'id': 2,
        'title': 'French onion soup',
        'serves': 4,
        'ingredients': [
            {
                'id': 1,
                'name': 'Onion',
                'quantity': {
                    'grams': 200.0
                }
            },
            {
                'id': 2,
                'name': 'Garlic',
                'quantity': {
                    'grams': 40.0
                }
            },
            {
                'id': 4,
                'name': 'Gruyère',
                'quantity': {
                    'grams': 200.0
                }
            },
            {
                'id': 5,
                'name': 'Balsamic vinegar',
                'quantity': {
                    'grams': 5.0
                }
            }
        ]
    }
]


def test_get_aggregate_ingredients():
    plan = [PlanItem('Monday', 'Lunch', recipes[0]),
            PlanItem('Monday', 'Dinner', recipes[1])]
    ingredients = get_aggregate_ingredients(plan)

    assert ingredients == [
        ('Onion', 300.0),
        ('Garlic', 70.0),
        ('Fresh ginger', 10.0),
        ('Gruyère', 200.0),
        ('Balsamic vinegar', 5.0)
    ]


def test_get_aggregate_ingredients_multiple_batches():
    plan = [PlanItem('Monday', 'Lunch', recipes[0]), PlanItem(
        'Monday', 'Dinner', recipes[0]), PlanItem('Tuesday', 'Lunch', recipes[0])]
    ingredients = get_aggregate_ingredients(plan)

    assert ingredients == [
        ('Onion', 200.0),
        ('Garlic', 60.0),
        ('Fresh ginger', 20.0),
    ]


def test_get_aggregate_ingredients_empty_plan():
    assert get_aggregate_ingredients([], recipes) == []


def test_get_aggregate_ingredients_with_inventory():
    plan = [PlanItem('Monday', 'Lunch', recipes[0])]
    inventory = {'Onion': 100.0}
    ingredients = get_aggregate_ingredients(plan, inventory)

    assert ingredients == [
        ('Onion', 0.0), ('Garlic', 30.0), ('Fresh ginger', 10.0)]


def test_get_leftover_recipe_portions():
    plan = [
        PlanItem('Monday', 'Lunch', recipes[1]),
        PlanItem('Tuesday', 'Lunch', recipes[1]),
        PlanItem('Wednesday', 'Lunch', recipes[1]),
        PlanItem('Wednesday', 'Dinner', recipes[1]),
        PlanItem('Thursday', 'Lunch', recipes[1]),
        PlanItem('Thursday', 'Dinner', recipes[0]),
        PlanItem('Friday', 'Dinner', recipes[0])
    ]

    assert get_leftover_recipe_portions(plan) == [(recipes[1], 3.0)]
