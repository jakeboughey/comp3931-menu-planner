from typing import *

from models.product import *
from products import select_products, get_products_for_ingredients

ingredients = ['tuna']

products = {
    'tuna': [
        Product({'id': 'tuna_a', 'product_name': 'tuna_a', 'gram_weight': 250.0}),
        Product({'id': 'tuna_b', 'product_name': 'tuna_b', 'gram_weight': 400.0}),
        Product({'id': 'tuna_c', 'product_name': 'tuna_c', 'gram_weight': 200.0})
    ]
}


def test_select_products(mocker):
    prods = products['tuna']
    mocker.patch('products.get_products_by_name',
                 return_value=prods)

    selected_quantities = select_products('tuna', 400.0)

    assert selected_quantities['products'] == {
        prods[0]: 0.0,
        prods[1]: 0.0,
        prods[2]: 2.0
    }


def test_select_products_for_zero_amount(mocker):
    prods = products['tuna']
    mocker.patch('products.get_products_by_name',
                 return_value=prods)
    selected_quantities = select_products('tuna', 0.0)

    assert selected_quantities['products'] == {
        prods[0]: 0.0,
        prods[1]: 0.0,
        prods[2]: 0.0
    }


def test_get_products_for_ingredients(mocker):
    prods = products['tuna']
    mocker.patch('products.get_products_by_name',
                 return_value=prods)

    measured_ingredients = [('tuna', 400.0)]
    actual_products = get_products_for_ingredients(measured_ingredients)

    assert actual_products[measured_ingredients[0][0]]['products'] == {
        prods[0]: 0.0,
        prods[1]: 0.0,
        prods[2]: 2.0
    }
