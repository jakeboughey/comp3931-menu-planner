import os

import pymongo

if __name__ == '__main__':
    # Get DB connection
    db = pymongo.MongoClient(os.getenv('MONGO_CONNECTION'))

    # Get distinct ingredient names used across all recipes
    ings = sorted(
        list(db.recipe1m.formatted_recipes.distinct('ingredients.name')))

    # For each ingredient, do search for products and specify
    # which found products match the ingredient
    for ing_idx, ing in enumerate(ings):
        while True:
            print('INGREDIENT ({}/{}): {}'.format(ing_idx, len(ings), ing))

            query = input('Search products> ')

            if query == '':
                continue
            elif query == 'next':
                break

            prods = list(db.off.restricted_products
                         .find({'product_name': {'$regex': query, '$options': 'i'}}, {'id': 1, 'product_name': 1})
                         .limit(10))

            for idx, prod in enumerate(prods):
                print('[{}] (id={}) {}'.format(
                    idx, prod['id'], prod['product_name']))

            keep_idxs = input('Products to keep, comma separated> ')

            if keep_idxs == 'search':
                continue
            elif keep_idxs == 'next':
                break

            try:
                keep_idxs = [int(idx) for idx in keep_idxs.split(',')]
            except ValueError:
                continue
            else:
                keep_prods = [{
                    'id': prods[idx]['id'],
                    'product_name': prods[idx]['product_name']
                } for idx in keep_idxs]

                db.recipe1m.ingredient_products.replace_one(
                    {'ingredient': ing},
                    {'ingredient': ing, 'products': keep_prods},
                    upsert=True)

                break
