import os

import pymongo
import pint

if __name__ == '__main__':
    ureg = pint.UnitRegistry()
    Q_ = ureg.Quantity
    ureg.define('[density] = [mass] / [volume]')

    # Get DB connection
    db = pymongo.MongoClient(os.getenv('MONGO_CONNECTION'))

    for product in db.off.restricted_products_copy_no_dimless.find():
        quantity = Q_(product['quantity']['amount'],
                      product['quantity']['unit'])

        if quantity.check('[mass]'):
            gram_weight = quantity.to('g')
            print(
                '(id={}) {} -> {}'.format(product['_id'], quantity, gram_weight))
            db.off.restricted_products_copy_no_dimless.update_one(
                {'_id': product['_id']}, {'$set': {'gram_weight': gram_weight.magnitude}})
        elif quantity.check('[volume]'):
            matched_ingredients = product['matched_ingredients']
            for ingredient in matched_ingredients:
                density_match = db.recipe1m.ingredient_products.find_one(
                    {'ingredient': ingredient})
                density_match_id = density_match['density_match_2']
                density = db.fooddata_central.food_density.find_one({'_id': density_match_id})[
                    'density_g_per_ml']
                gram_weight = Q_(density, 'g / ml') * quantity.to('ml')
                print(
                    '(id={}) {} -> {}'.format(product['_id'], quantity, gram_weight))
                db.off.restricted_products_copy_no_dimless.update_one(
                    {'_id': product['_id']}, {'$set': {'gram_weight': gram_weight.magnitude}})
        else:
            raise ValueError(
                'Product quantity must be mass or volume (offender={})'.format(product['_id']))
