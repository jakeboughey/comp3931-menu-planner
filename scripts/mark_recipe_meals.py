import os

import pymongo
from termcolor import cprint

if __name__ == '__main__':
    # Get DB connection
    db = pymongo.MongoClient(os.getenv('MONGO_CONNECTION'))

    recipes = list(db.recipe1m.formatted_recipes_ing_filtered.find(
        {'meal_types': {'$exists': False}}))

    for idx, recipe in enumerate(recipes):
        while True:
            cprint('\n\n[{}/{}] "{}"'.format(idx,
                                             len(recipes), recipe['title']), 'red')

            meal_types_input = input('Meal types> ')
            meal_types = []

            if meal_types_input == '':
                break

            if meal_types_input == 'del':
                db.recipe1m.formatted_recipes_ing_filtered.delete_one(
                    {'id': recipe['id']})
                break

            if not all(char in 'bld' for char in meal_types_input):
                continue

            if 'b' in meal_types_input:
                meal_types.append('breakfast')
            if 'l' in meal_types_input:
                meal_types.append('lunch')
            if 'd' in meal_types_input:
                meal_types.append('dinner')

            db.recipe1m.formatted_recipes_ing_filtered.update_one(
                {'id': recipe['id']}, {'$set': {'meal_types': meal_types}})

            break
