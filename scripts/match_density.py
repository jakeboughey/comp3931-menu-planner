import os

import pymongo
from termcolor import cprint

if __name__ == '__main__':
    # Get DB connection
    db = pymongo.MongoClient(os.getenv('MONGO_CONNECTION'))

    # Get distinct ingredient names used across all recipes
    # (ing_filtered collection already excludes recipes whose ingredients have no prods available)
    ings = sorted(
        list(db.recipe1m.formatted_recipes_ing_filtered.distinct('ingredients.name')))

    # For each ingredient, do search for density record
    # and select closest
    #
    # e.g. for soy milk we may use the density record for dairy
    # milk as they have similar densities
    for ing_idx, ing in enumerate(ings):
        while True:
            print('\n\n')
            print('INGREDIENT ({}/{}): {}'.format(ing_idx, len(ings), ing))

            query = input('Search density records> ')

            if query == '':
                continue
            elif query == 'next':
                break
            elif query == '*':
                query = ing

            closest_records = list(db.fooddata_central.food_density.find(
                {'original_name': {'$regex': query, '$options': 'i'}}).limit(20))

            for idx, record in enumerate(closest_records):
                cprint('[{}] {} ({} g/ml)'.format(
                    idx, record['original_name'], record['density_g_per_ml']), 'red')

            keep_idx = input('Closest record index> ')

            if keep_idx == 'next':
                break

            try:
                keep_record = closest_records[int(keep_idx)]
            except:
                continue
            else:
                db.recipe1m.ingredient_products.update_one(
                    {'ingredient': ing},
                    {'$set': {'density_match_2': keep_record['_id']}},
                    upsert=True)

                break
